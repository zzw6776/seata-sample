/*
 * Copyright © ${project.inceptionYear} organization baomidou
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.baomidou.samples.seata.controller;

import com.baomidou.samples.seata.dto.PlaceOrderRequest;
import com.baomidou.samples.seata.service.OrderService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
public class OrderController{

    @Autowired
    private OrderService orderService;
//    @Value("${testValue}")
//    String testValue;

    @GetMapping("/order/placeOrder")
    public String placeOrder(@Validated @RequestBody PlaceOrderRequest request) {
        //log.info(testValue);
        orderService.placeOrder(request);
        return "下单成功";
    }


    @ApiOperation("测试商品库存不足-异常回滚")
    @GetMapping("/order/test1")
    public String test1() {
        //商品单价10元，库存20个,用户余额50元，模拟一次性购买22个。 期望异常回滚
        orderService.placeOrder(new PlaceOrderRequest(1L, 1L, 22));
        return "下单成功";
    }


    @ApiOperation("测试用户账户余额不足-异常回滚")
    @GetMapping("/order/test2")
    public String test2() {
        //商品单价10元，库存20个，用户余额50元，模拟一次性购买6个。 期望异常回滚
        orderService.placeOrder(new PlaceOrderRequest(1L, 1L, 6));
        return "下单成功";
    }
}